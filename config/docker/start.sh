#!/bin/sh

/usr/local/bin/pm2 start /app/app.js
/usr/sbin/nginx -g 'daemon off;' -c /etc/nginx/nginx.conf
