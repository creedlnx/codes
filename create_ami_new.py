#!/usr/bin/env python

import boto3
import time
import sys

sessionSaRegion = boto3.Session(profile_name='profile-sa')
ec2SaResource = sessionSaRegion.resource('ec2')
ec2SaClient = sessionSaRegion.client('ec2')

sessionUsRegion = boto3.Session(profile_name='profile-us')
ec2UsClient = sessionUsRegion.client('ec2')

serviceTagName = str(sys.argv[1])

InstanceIdList = []
InstanceTagName = []

count = 0

def GetInstanceId(serviceTag):
    id_instances = ec2SaResource.instances.filter(
        Filters=[{'Name': 'tag-value', 'Values': [serviceTagName]}])
    for instance in id_instances:
        InstanceIdList.append(instance.id)
    return
GetInstanceId(InstanceIdList)

def GetInstanceName():
    instanceName = ec2SaClient.describe_instances()
    for idLists in InstanceIdList:
        for reservations in instanceName['Reservations']:
            for instance in reservations['Instances']:
                if instance['InstanceId'] == idLists:
                    InstanceTagName.append(instance['Tags'][0]['Value'])
    return
GetInstanceName()

def GetAMIId():
    getAmiId = ec2SaResource.images.filter(
    Filters=[
        {
            'Name': 'name',
            'Values': [
                'AMI-' + InstanceIdList[count] + '-' + InstanceTagName[count] + '-' + time.strftime("%d%m%Y"),
            ]
        },
    ],
    )

    for idAmi in getAmiId:
        AmiIdList = []
        AmiIdList.insert(0,i.id)
        print AmiIdList[0]
    return

amiId = GetAMIId()

for instId in InstanceIdList:
    instance = ec2SaResource.Instance(InstanceIdList[count])
    image = instance.create_image(
      BlockDeviceMappings=[
        {"DeviceName": "xvdc", "NoDevice": ""},
        {"DeviceName": "xvdd", "NoDevice": ""},
        {"DeviceName": "xvde", "NoDevice": ""},
        {"DeviceName": "xvdf", "NoDevice": ""},
        {"DeviceName": "xvdg", "NoDevice": ""},
        {"DeviceName": "xvdh", "NoDevice": ""}
        ],
      Description='AMI para' + ' ' + InstanceIdList[count] + '-' +  InstanceTagName[count] + ' Data ' + time.strftime("%d%m%Y"),
      Name='AMI-' + InstanceIdList[count] + '-' + InstanceTagName[count] + '-' + time.strftime("%d%m%Y"),
      NoReboot=True
      )
    count = count + 1

    response = ec2UsClient.copy_image(
    Description='AMI para' + ' ' + InstanceIdList[count] + '-' +  InstanceTagName[count] + ' Data ' + time.strftime("%d%m%Y"),
    Name='AMI-' + InstanceIdList[count] + '-' + InstanceTagName[count] + '-' + time.strftime("%d%m%Y"),
    SourceImageId=amiId,
    SourceRegion='sa-east-1'
    )