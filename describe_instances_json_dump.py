#!/usr/bin/env python

import boto3
import json
import datetime
from jq import jq
from subprocess import check_output

def datetime_handler(x):
    if isinstance(x, datetime.datetime):
        return x.isoformat()
    raise TypeError("Unknown type")

ec2 = boto3.client('ec2', region_name='us-east-1')
boto3.setup_default_session(profile_name='default')

instDict = ec2.describe_instances()

data = json.dumps(instDict, default=datetime_handler)
print (data)