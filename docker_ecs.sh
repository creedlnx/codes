#!/bin/bash

sudo yum install -y postgresql96 docker jq
sudo /etc/init.d/docker start

AWSVPC_CIDR=`aws ec2 describe-vpcs | jq -c '.Vpcs[] | select(.IsDefault | contains(true)).CidrBlock' | tr -d \"`
AWSVPC_ID=`aws ec2 describe-vpcs | jq -c '.Vpcs[] | select(.IsDefault | contains(true)).VpcId' | tr -d \"`
AWSSG_SECGROUPNAME="secgrprds_ebn_andersonnunes"
AWSSG_SECGROUPID=`aws ec2 create-security-group --description $AWSSG_SECGROUPNAME --group-name $AWSSG_SECGROUPNAME --vpc-id $AWSVPC_ID | jq .GroupId | tr -d \"`
AWSRDS_INSTANCENAME="rdsinst_ebn_andersonnunes"
AWSRDS_DBINSTANCE=`echo $AWSRDS_INSTANCENAME | tr -d \_`
AWSRDS_MASTERUSER="postgres"
AWSRDS_MASTERPASS="Jqrp9e3ZLZeTC"
AWSECS_CLUSTER="ecscluster_ebn_andersonnunes"

export PGPASSWORD=$AWSRDS_MASTERPASS

aws ec2 create-key-pair --key-name ebn_keypair --query 'KeyMaterial' --output text > ebn_keypair.pem
chmod 400 ebn_keypair.pem

cat <<EOF>> ec2config.txt
#!/bin/bash
echo ECS_CLUSTER=$AWSECS_CLUSTER >> /etc/ecs/ecs.config
EOF

jq -n '{ "Statement": [{ "Sid": "", "Effect": "Allow", "Principal": { "Service": "ec2.amazonaws.com" }, "Action": "sts:AssumeRole" }]}' > ecsPolicy.json
jq -n '{"Statement":[{"Effect":"Allow","Action":["ecs:CreateCluster","ecs:DeregisterContainerInstance","ecs:DiscoverPollEndpoint","ecs:Poll","ecs:RegisterContainerInstance","ecs:StartTelemetrySession","ecs:UpdateContainerInstancesState","ecs:Submit*","ecr:GetAuthorizationToken","ecr:BatchCheckLayerAvailability","ecr:GetDownloadUrlForLayer","ecr:BatchGetImage","logs:CreateLogStream","logs:PutLogEvents"],"Resource":"*"}]}' > rolePolicy.json

aws iam create-role --role-name ecsRole --assume-role-policy-document file://ecsPolicy.json
aws iam put-role-policy --role-name ecsRole --policy-name ecsRolePolicy  --policy-document file://rolePolicy.json
aws iam create-instance-profile --instance-profile-name ecsInstanceRole
aws iam add-role-to-instance-profile --instance-profile-name ecsInstanceRole --role-name ecsRole

aws ec2 authorize-security-group-ingress --group-name $AWSSG_SECGROUPNAME --protocol tcp --port 5432 --cidr $AWSVPC_CIDR
aws ec2 authorize-security-group-ingress --group-name $AWSSG_SECGROUPNAME --protocol tcp --port 9080 --cidr $AWSVPC_CIDR
aws ec2 authorize-security-group-ingress --group-name $AWSSG_SECGROUPNAME --protocol tcp --port 80 --cidr $AWSVPC_CIDR
aws ec2 authorize-security-group-ingress --group-name $AWSSG_SECGROUPNAME --protocol tcp --port 22 --cidr $AWSVPC_CIDR

aws ec2 run-instances --image-id ami-ba722dc0 --count 1 --instance-type t2.micro --key-name ebn_keypair --iam-instance-profile "{\"Name\":\"ecsInstanceRole\"}" --tag-specifications "[{\"ResourceType\":\"instance\",\"Tags\":[{\"Value\":\"ebn_ec2_instance\",\"Key\":\"Name\"}]}]" --security-groups $AWSSG_SECGROUPNAME --user-data file://ec2config.txt

aws rds create-db-instance --db-name $AWSRDS_INSTANCENAME --db-instance-identifier $AWSRDS_DBINSTANCE --allocated-storage 8 \
--db-instance-class db.t2.micro --engine postgres --master-username $AWSRDS_MASTERUSER --master-user-password "$AWSRDS_MASTERPASS" \
--vpc-security-group-ids $AWSSG_SECGROUPID --no-publicly-accessible --no-auto-minor-version-upgrade --no-multi-az --engine-version 9.6.3

AWSRDS_ENDPOINT=`aws rds describe-db-instances --db-instance-identifier $AWSRDS_DBINSTANCE | jq .DBInstances[0].Endpoint.Address | tr -d \"`
AWSRDS_STATUS=`aws rds describe-db-instances --db-instance-identifier $AWSRDS_DBINSTANCE | jq .DBInstances[0].DBInstanceStatus | tr -d \"`

time \
while [ "$AWSRDS_ENDPOINT" == "null" ]; do
    sleep 10
    AWSRDS_ENDPOINT=`aws rds describe-db-instances --db-instance-identifier $AWSRDS_DBINSTANCE | jq .DBInstances[0].Endpoint.Address | tr -d \"`
    echo "Aguardando Formacao do RDS para coletar o End Point"
    echo " "
    AWSRDS_STATUS=`aws rds describe-db-instances --db-instance-identifier $AWSRDS_DBINSTANCE | jq .DBInstances[0].DBInstanceStatus | tr -d \"`
    echo "Status" $AWSRDS_STATUS
done

echo "End Point disponivel: " $AWSRDS_ENDPOINT

echo "Verificando se a instancia ja esta disponivel."

time \
while [ "$AWSRDS_STATUS" != "available" ]; do
    AWSRDS_STATUS=`aws rds describe-db-instances --db-instance-identifier $AWSRDS_DBINSTANCE | jq .DBInstances[0].DBInstanceStatus | tr -d \"`
    echo "Status atual da instancia: " $AWSRDS_STATUS
    echo " "
    sleep 10
done

psql -h $AWSRDS_ENDPOINT -U $AWSRDS_MASTERUSER -d $AWSRDS_INSTANCENAME < acesso_init.sql

AWSECR_ACCID_ADDRESS=`aws ecr get-login --no-include-email | awk -F '//' '{ print $2 }'`
AWS_ACCID=`aws ecr get-login --no-include-email | awk -F '//' '{ print $2 }' | cut -d '.' -f1`
aws ecr get-login --no-include-email | sudo su - -c sh
aws ecr create-repository --repository-name ebn_spring_app

cat <<EOF>> Dockerfile
FROM centos
RUN yum -y install wget
ADD jdk-8u151-linux-x64.rpm /tmp/jdk-8-linux-x64.rpm
RUN yum -y install /tmp/jdk-8-linux-x64.rpm
ENV JAVA_HOME /usr/java/latest
ENV DATASOURCE_URL=jdbc:postgresql://$AWSRDS_ENDPOINT:5432/$AWSRDS_INSTANCENAME
ENV DATASOURCE_USERNAME=$AWSRDS_MASTERUSER
ENV DATASOURCE_PASSWORD=$AWSRDS_MASTERPASS
ADD acesso.jar /srv/acesso.jar
CMD /usr/java/latest/bin/java -Dspring.datasource.url=$DATASOURCE_URL -Dspring.datasource.username=$DATASOURCE_USERNAME -Dspring.datasource.password=$DATASOURCE_PASSWORD -jar /srv/acesso.jar
EOF

sudo docker build -t imgebn_spring_app .
sudo docker tag imgebn_spring_app $AWSECR_ACCID_ADDRESS/ebn_spring_app
sudo docker push $AWSECR_ACCID_ADDRESS/ebn_spring_app

aws ecs create-cluster --cluster-name "$AWSECS_CLUSTER"

AWSEC2_INSTID=`aws ec2 describe-tags | jq -c '.Tags[] | select(.Value | contains("ebn_ec2_instance")).ResourceId' | tr -d \"`

aws ecs list-container-instances --cluster $AWSECS_CLUSTER
aws elb create-load-balancer --load-balancer-name  elbspringapp --listeners "Protocol=HTTP,LoadBalancerPort=80,InstanceProtocol=HTTP,InstancePort=9080" --availability-zones "us-east-1a" "us-east-1b" "us-east-1c" "us-east-1d" "us-east-1f" --security-groups $AWSSG_SECGROUPID
aws elb register-instances-with-load-balancer --load-balancer-name elbspringapp --instances $AWSEC2_INSTID
aws ecs register-task-definition --family ebn_spring_app --network-mode "bridge" --container-definitions "[{ \"name\": \"ebn_docker\", \"image\": \"$AWSECR_ACCID_ADDRESS/ebn_spring_app:latest\", \"cpu\": 256, \"memory\": 512, \"portMappings\": [{ \"containerPort\": 9095, \"hostPort\": 9080 }] }]" --requires-compatibilities "EC2"
aws ecs create-service --cluster $AWSECS_CLUSTER --service-name ecsservice_ebn_andersonnunes --task-definition ebn_spring_app --load-balancers "[{ \"loadBalancerName\": \"elbspringapp\", \"containerName\": \"ebn_docker\", \"containerPort\": 9095 }]" --desired-count 1 --launch-type EC2